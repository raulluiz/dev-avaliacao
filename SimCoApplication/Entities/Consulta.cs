﻿using SimCoApplication.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SimCoApplication.Entities
{
    public class Consulta
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public EnumDiaDaSemana DiaDaSemana { get; set; }
        public int IdPaciente { get; set; }
        public int IdMedico { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }
        public bool Realizada { get; set; }
        public bool Cancelada { get; set; }

        public Consulta()
        {

        }

        public Consulta(EnumDiaDaSemana diaDaSemana, int idPaciente, int idMedico, DateTime dataInicio, DateTime dataFim, bool realizada, bool cancelada)
        {
            this.DiaDaSemana = diaDaSemana;
            this.IdPaciente = idPaciente;
            this.IdMedico = idMedico;
            this.DataInicio = dataInicio;
            this.DataFim = dataFim;
            this.Realizada = realizada;
            this.Cancelada = cancelada;
        }

        public void AlterarHorario(DateTime dataInicio, DateTime dataFim)
        {
            this.DataInicio = dataInicio;
            this.DataFim = dataFim;
        }

        public void RealizarConsulta()
        {
            this.Realizada = true;
        }

        public void CancelarConsulta()
        {
            this.Cancelada = true;
        }

    }
}
