﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SimCoApplication.Entities
{
    public class Medico
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Nome { get; set; }
        public long CRM { get; set; }
        public bool Ativo { get; set; }

        public Medico()
        {

        }
        public Medico(string nome, long crm)
        {
            this.Nome = nome;
            this.CRM = crm;
        }

        public void Alterar(string nome, long crm)
        {
            this.Nome = nome;
            this.CRM = crm;
        }

        public void Inativar()
        {
            this.Ativo = false;
        }

    }
}
