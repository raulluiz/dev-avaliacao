﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Transactions;

namespace SimCoApplication.Entities
{
    public class Paciente
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Nome { get; set; }
        public bool Ativo { get; set; }
        public string CPF { get; set; }

        public Paciente()
        {

        }
        public Paciente(string nome, string cpf)
        {
            this.Nome = nome;
            this.CPF = cpf;
        }

        public void Alterar(string nome, string cpf)
        {
            this.Nome = nome;
            this.CPF = cpf;
        }

        public void Inativar()
        {
            this.Ativo = false;
        }
    }
}
