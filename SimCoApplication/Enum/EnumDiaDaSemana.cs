﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SimCoApplication.Enum
{
    public enum EnumDiaDaSemana : byte
    {
        [Display(Name = "Segunda")]
        Segunda = 1,

        [Display(Name = "Terça-Feira")]
        Terca = 2,

        [Display(Name = "Quarta-Feira")]
        Quarta = 3,

        [Display(Name = "Quinta-Feira")]
        Quinta = 4,

        [Display(Name = "Sexta-Feira")]
        Sexta = 5,

        [Display(Name = "Sábado")]
        Sabado = 6,

        [Display(Name = "Domingo")]
        Domingo = 7

    }
}
