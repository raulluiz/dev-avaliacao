﻿using SimCoApplication.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimCoApplication.Interfaces.Repository
{
    public interface IPacienteRepository : IBaseRepository<Paciente>
    {
    }
}
