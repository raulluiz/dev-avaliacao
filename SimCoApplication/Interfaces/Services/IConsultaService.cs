﻿using SimCoApplication.Entities;
using SimCoApplication.Services;
using SimCoApplication.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimCoApplication.Interfaces.Services
{
    public interface IConsultaService : IBaseService<Consulta>
    {
        void Cancelar(int id);
        void EditarConsulta(ConsultaVM consultaVM);
        void SalvarConsulta(ConsultaVM consultaVM);
    }
}
