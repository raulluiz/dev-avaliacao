﻿using SimCoApplication.Entities;
using SimCoApplication.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimCoApplication.Interfaces.Services
{
    public interface IMedicoService : IBaseService<Medico>
    {
        void SalvarMedico(MedicoVM medicoVM);
        void EditarMedico(MedicoVM medicoVM);
        void InativarMedico(int id);
    }
}
