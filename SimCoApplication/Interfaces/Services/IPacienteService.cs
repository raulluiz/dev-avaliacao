﻿using SimCoApplication.Entities;
using SimCoApplication.Services;
using SimCoApplication.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimCoApplication.Interfaces.Services
{
    public interface IPacienteService : IBaseService<Paciente>
    {
        void SalvarPaciente(PacienteVM pacienteVM);
        void EditarPaciente(PacienteVM pacienteVM);
        void InativarPaciente(int id);
    }
}
