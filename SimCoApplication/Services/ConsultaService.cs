﻿using SimCoApplication.Entities;
using SimCoApplication.Interfaces.Repository;
using SimCoApplication.Interfaces.Services;
using SimCoApplication.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimCoApplication.Services
{
    public class ConsultaService : BaseService<Consulta>, IConsultaService
    {
        private readonly IConsultaRepository _consultaRepository;

        public ConsultaService(IConsultaRepository consultaRepository) : base(consultaRepository)
        {
            _consultaRepository = consultaRepository;
        }

        public void SalvarConsulta(ConsultaVM consultaVM)
        {
            var consulta = _consultaRepository.Add(new Consulta(consultaVM.DiaDaSemana, consultaVM.IdPaciente, consultaVM.IdMedico, consultaVM.DataInicio, consultaVM.DataFim, consultaVM.Realizada, consultaVM.Cancelada));
            _consultaRepository.Update(consulta);
        }

        public void EditarConsulta(ConsultaVM consultaVM)
        {
            var consulta = _consultaRepository.GetById(consultaVM.Id);
            consulta.AlterarHorario(consultaVM.DataInicio, consultaVM.DataFim);
            _consultaRepository.Update(consulta);
        }

        public void Cancelar(int id)
        {
            var consulta = _consultaRepository.GetById(id);
            consulta.CancelarConsulta();
            _consultaRepository.Update(consulta);
        }

    }
}
