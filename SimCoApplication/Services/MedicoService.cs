﻿using SimCoApplication.Entities;
using SimCoApplication.Interfaces.Repository;
using SimCoApplication.Interfaces.Services;
using SimCoApplication.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimCoApplication.Services
{

    public class MedicoService : BaseService<Medico>, IMedicoService
    {
        private readonly IMedicoRepository _medicoRepository;

        public MedicoService(IMedicoRepository medicoRepository) : base(medicoRepository)
        {
            _medicoRepository = medicoRepository;
        }

        public void SalvarMedico(MedicoVM medicoVM)
        {
            var medico = _medicoRepository.Add(new Medico(medicoVM.Nome, medicoVM.CRM));
            _medicoRepository.Update(medico);
        }

        public void EditarMedico(MedicoVM medicoVM)
        {
            var medico = _medicoRepository.GetById(medicoVM.Id);
            medico.Alterar(medicoVM.Nome, medicoVM.CRM);
            _medicoRepository.Update(medico);
        }

        public void InativarMedico(int id)
        {
            var medico = _medicoRepository.GetById(id);
            medico.Inativar();
            _medicoRepository.Update(medico);
        }

    }
}
