﻿using SimCoApplication.Entities;
using SimCoApplication.Interfaces.Repository;
using SimCoApplication.Interfaces.Services;
using SimCoApplication.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimCoApplication.Services
{
    public class PacienteService : BaseService<Paciente>, IPacienteService
    {
        private readonly IPacienteRepository _pacienteRepository;

        public PacienteService(IPacienteRepository pacienteRepository) : base(pacienteRepository)
        {
            _pacienteRepository = pacienteRepository;
        }

        public void SalvarPaciente(PacienteVM pacienteVM)
        {
            var paciente = _pacienteRepository.Add(new Paciente(pacienteVM.Nome, pacienteVM.CPF));
            _pacienteRepository.Update(paciente);
        }

        public void EditarPaciente(PacienteVM pacienteVM)
        {
            var paciente = _pacienteRepository.GetById(pacienteVM.Id);
            paciente.Alterar(pacienteVM.Nome, pacienteVM.CPF);
            _pacienteRepository.Update(paciente);
        }

        public void InativarPaciente(int id)
        {
            var paciente = _pacienteRepository.GetById(id);
            paciente.Inativar();
            _pacienteRepository.Update(paciente);
        }

    }
}
