﻿using SimCoApplication.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimCoApplication.ViewModels
{
    public class ConsultaVM
    {
        public int Id { get; set; }
        public EnumDiaDaSemana DiaDaSemana { get; set; }
        public int IdPaciente { get; set; }
        public int IdMedico { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }
        public bool Realizada { get; set; }
        public bool Cancelada { get; set; }
    }
}
