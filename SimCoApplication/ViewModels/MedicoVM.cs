﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimCoApplication.ViewModels
{
    public class MedicoVM
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public long CRM { get; set; }
        public bool Ativo { get; set; }
    }
}
