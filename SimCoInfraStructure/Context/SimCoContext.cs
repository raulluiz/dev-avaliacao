﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using SimCoApplication.Entities;
using SimCoInfraStructure.EntityConfig;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SimCoInfraStructure.Context
{
    public class SimCoContext : DbContext
    {
        public SimCoContext(){}
        public SimCoContext(DbContextOptions<SimCoContext> opcoes) : base(opcoes) { }

        public DbSet<Paciente> Pacinte { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.ApplyConfiguration.Remove<PluralizingTableNameConvention>(); //plularização de objetos
            //modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>(); //deleção em cascata de filho
            modelBuilder.ApplyConfiguration(new PacienteConfig());
            modelBuilder.ApplyConfiguration(new MedicoConfig());
            modelBuilder.ApplyConfiguration(new ConsultaConfig());

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
            optionsBuilder.UseNpgsql(config.GetConnectionString("DefaultConnection"));
        }
    }
}
