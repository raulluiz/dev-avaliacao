﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SimCoApplication.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimCoInfraStructure.EntityConfig
{
    public class ConsultaConfig : IEntityTypeConfiguration<Consulta>
    {
        public void Configure(EntityTypeBuilder<Consulta> builder)
        {
            builder.ToTable("Consulta");
            builder.HasKey(c => c.Id);
            builder.Property(c => c.Id).ValueGeneratedOnAdd();
            builder.Property(c => c.IdMedico);
            builder.Property(c => c.IdPaciente);
            builder.Property(c => c.DataInicio);
            builder.Property(c => c.DataFim);
            builder.Property(c => c.DiaDaSemana);
            builder.Property(c => c.Cancelada);
            builder.Property(c => c.Realizada);
        }
    }
}
