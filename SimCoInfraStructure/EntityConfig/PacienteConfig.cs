﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SimCoApplication.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimCoInfraStructure.EntityConfig
{
    public class PacienteConfig : IEntityTypeConfiguration<Paciente>
    {
        public void Configure(EntityTypeBuilder<Paciente> builder)
        {
            builder.ToTable("Paciente");
            builder.HasKey(c => c.Id);
            builder.Property(c => c.Id).ValueGeneratedOnAdd();
            builder.Property(c => c.Nome);
            builder.Property(c => c.Ativo);
            builder.Property(c => c.CPF);
        }
    }
}
