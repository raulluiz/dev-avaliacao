﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SimCoInfraStructure.Migrations
{
    public partial class AtivoPaciente : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Ativo",
                table: "Paciente",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Ativo",
                table: "Paciente");
        }
    }
}
