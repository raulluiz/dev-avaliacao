﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SimCoInfraStructure.Migrations
{
    public partial class CPFPaciente : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CPF",
                table: "Paciente",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CPF",
                table: "Paciente");
        }
    }
}
