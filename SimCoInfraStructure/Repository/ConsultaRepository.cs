﻿using SimCoApplication.Entities;
using SimCoApplication.Interfaces.Repository;
using SimCoInfraStructure.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimCoInfraStructure.Repository
{
    public class ConsultaRepository : BaseRepository<Consulta>, IConsultaRepository
    {
        public ConsultaRepository(SimCoContext dbContext) : base(dbContext)
        {
        }
    }
}
