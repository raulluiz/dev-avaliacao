﻿using SimCoApplication.Entities;
using SimCoApplication.Interfaces.Repository;
using SimCoInfraStructure.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimCoInfraStructure.Repository
{
    public class MedicoRepository : BaseRepository<Medico>, IMedicoRepository
    {
        public MedicoRepository(SimCoContext dbContext) : base(dbContext)
        {
        }
    }
}
