﻿using SimCoApplication.Entities;
using SimCoApplication.Interfaces.Repository;
using SimCoInfraStructure.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace SimCoInfraStructure.Repository
{
    public class PacienteRepository : BaseRepository<Paciente>, IPacienteRepository
    {
        public PacienteRepository(SimCoContext dbContext) : base(dbContext)
        {
        }
    }
}
