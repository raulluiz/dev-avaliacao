﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SimCoApplication.Interfaces.Services;
using SimCoApplication.ViewModels;

namespace SimCoWeb.Controllers
{
    public class PacienteController : Controller
    {
        #region Injeção de Dependência
        private readonly IPacienteService _pacienteService;
        public PacienteController(IPacienteService pacienteService)
        {
            _pacienteService = pacienteService;
        }
        #endregion

        public IActionResult Index()
        {
            try
            {
                var pacientes = _pacienteService.GetAll();
                return Ok(pacientes);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
                throw;
            }
            
        }

        public IActionResult Create()
        {
            return Ok();
        }

        [HttpPost]
        public IActionResult Create(PacienteVM paciente)
        {
            try
            {
                _pacienteService.SalvarPaciente(paciente);

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
                throw;
            }
            
        }

        public IActionResult Editar(int id)
        {
            try
            {
                var paciente = _pacienteService.GetById(id);
                return Ok(paciente);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
                throw;
            }
            
        }

        [HttpPost]
        public IActionResult Editar(PacienteVM paciente)
        {
            try
            {
                _pacienteService.EditarPaciente(paciente);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
                throw;
            }
            
        }

        [HttpPost]
        public IActionResult Excluir(int id)
        {
            try
            {
                _pacienteService.InativarPaciente(id);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
                throw;
            }
            
        }
    }
}
